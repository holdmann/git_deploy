<?php

/**
 * @file
 * Administrative callbacks for the Git deploy module.
 */

/**
 * Builds Git deploy settings form.
 */
function git_deploy_settings_form($form, &$form_state) {
  $form['git_deploy_path_to_git'] = array(
    '#type' => 'textfield',
    '#title' => t('Path to git executable'),
    '#default_value' => variable_get('git_deploy_path_to_git', 'git'),
    '#description' => t('The Git deploy path to git executable.'),
  );

  return system_settings_form($form);
}